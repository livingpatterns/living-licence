Living License 1.0.0
--------------------

The _**Living License**_ is a simple, permissive copyright license derived from the _[Blue Oak Model License](https://blueoakcouncil.org/license/1.0.0)_.  
  
_The intention of the Living License is to permit **‘You’**, (an entity that is **not** a corporation, partnership, incorporated association, joint venture, business trust, limited liability company, joint stock company, or group of companies with more than two hundred and fifty employees) to **‘Use This Work’**, (do anything with work made available under these terms)_ while _**‘Workers’**, (the persons who are offering work under these terms)_ disclaim liability.  
  
To receive this license, You must agree to the rules of the license which are obligations under that agreement and the conditions of the license. This means You must not Use This Work that creates rules that You cannot or will not follow.  
  
Workers license You to Use This Work that would otherwise infringe Workers copyright, or any patent claims Workers can license or become able to license.  
  
You must ensure that everyone that does Use This Work gets the text of this license, or a clickable link to: https://livinglicense.com.  
  
Workers cannot revoke this license. If Workers notify You that You have not complied with the rules of the license, You can still keep your license by complying within 30 days after the notice. If You do not do so, your license ends immediately.  
  
_**When You Use This Work, it is without any warranty or condition. Workers will not be liable to anyone for any damages related if You Use This Work or this license, under any kind of legal claim, as far as the law allows.**_
